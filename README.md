# mydud

Basic example of how to use [Driver Update Disk plugin](https://pagure.io/koji/pull-request/3217) on Koji. 


```
koji add-tag --arches "x86_64 aarch64" mytag8s-build
koji add-group mytag8s-build dud-build
koji add-group-pkg mytag8s-build dud-build xorriso
koji add-group-pkg mytag8s-build dud-build createrepo_c
koji add-group-pkg mytag8s-build dud-build dnf
koji add-group-pkg mytag8s-build dud-build dnf-plugins-core

koji edit-tag mytag8s-build -x mock.new_chroot=False

koji add-pkg mytag8s-testing mydud
koji add-pkg --owner linuxci mytag8s-testing mydud
koji add-pkg --owner linuxci mytag8s-qa mydud
koji add-pkg --owner linuxci mytag8s-stable mydud
koji dud-build mytag8s --scmurl=git+https://gitlab.cern.ch/linuxsupport/mydud.git#master mydud 1 myrpm

[djuarezg@aiadm08 ~]$ koji taskinfo 2360134
Task: 2360134
Type: dudBuild
Owner: djuarezg
State: closed
Created: Fri Mar  4 17:12:48 2022
Started: Fri Mar  4 17:12:52 2022
Finished: Fri Mar  4 17:17:18 2022
Host: koji33.cern.ch
Build: mydud-1-2 (54954)

koji download-build mydud-1-2 --type=image
```
